# swift-service-parser
基于[swift](https://github.com/facebook/swift)对一个swift service类进行解析返回metadata(ThriftServiceMetadata实例),可用于模板生成代码如(velocity)

# 命令行演示

>
	$java -jar swift-service-parser-1.0-SNAPSHOT-standalone.jar
	Usage: SwiftServiceParser [options] Swift-service-class-name
	  Options:
	    -classpath
	      semicolon-separated list of jar file or class folder
	      Default: <empty string>
	    -libdir
	      jar library folder
	      Default: []
	    -recursive
	      recursive search subdirectory for jar files,effective if -libdirs
	      specified
	      Default: false
