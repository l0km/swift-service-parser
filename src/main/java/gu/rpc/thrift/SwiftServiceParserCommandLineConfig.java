/*
 * Copyright (C) 2012 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package gu.rpc.thrift;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.beust.jcommander.IParameterValidator;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;

public class SwiftServiceParserCommandLineConfig
{
	public static class FolderValidator implements IParameterValidator {
		@Override
		public void validate(String name, String value) throws ParameterException {
			if(!(new File(value).isDirectory()))
				throw new ParameterException(String.format("parameter [%s] [%] is not folder", name,value));		
		}		
	}
	public static class ClasspathValidator implements IParameterValidator {
		@Override
		public void validate(String name, String value) throws ParameterException {
			String[] classpath = value.split(";");
			for(String p:classpath){
				File path = new File(p);
				if(path.isFile() && ! path.getName().endsWith(".jar"))
					throw new ParameterException(String.format("parameter [%s] [%] is invalide classpath", name,value));
			}
		}		
	}
    @Parameter(description = "Swift-service-class-name")
    public String classname;

    @Parameter(
            names = "-classpath",
            description = "semicolon-separated list of jar file or class folder",
            validateWith = ClasspathValidator.class
    )
    public String classpath = "";

    @Parameter(
            names = "-libdir",
            description = "jar library folder",
            validateWith  = FolderValidator.class
    )
    public List<String> libdirs = new ArrayList<String>() ;

	@Parameter(
	        names = "-recursive",
	        description = "recursive search subdirectory for jar files,effective if -libdirs specified"
	        
	)
	public boolean recursive = false;
}
