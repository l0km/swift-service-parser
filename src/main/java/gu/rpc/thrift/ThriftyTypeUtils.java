package gu.rpc.thrift;

import java.lang.reflect.Type;
import java.nio.ByteBuffer;

import com.google.common.reflect.TypeToken;

import okio.ByteString;

public class ThriftyTypeUtils extends TypeUtils {

	public ThriftyTypeUtils() {
	}
	protected boolean isByteString(Type type){
		return ByteString.class == type;
	}
	@Override
	public boolean isBinary(Type type) {
		return isByteBuffer(type) || isByteString(type) || byte[].class == type;
	}
	@Override
	public String asSwiftDeclareType(Type type,Boolean isGeneric){
		if(Boolean.TRUE.equals(isGeneric) && isBinary(type)){
			return "Object";
		}else{
			return asJavaType(type);
		}
	}
	@Override
	public Type toCallType(Type type){
		return type;
	}
	@Override
	public Type toPrimitive(Type type){
		TypeToken<?> typeToken = TypeToken.of(type);
		return typeToken.unwrap().getType();
	}
	@Override
	public String asSwiftType(Type thriftType,String prefix){
		if(thriftType == ByteBuffer.class){
			return ByteString.class.getName();
		}
		return asJavaType(thriftType,prefix);
	}
}
