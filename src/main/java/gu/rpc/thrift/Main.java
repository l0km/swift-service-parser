/*
 * Copyright (C) 2012 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */
package gu.rpc.thrift;

import com.beust.jcommander.JCommander;
import com.facebook.swift.service.metadata.ThriftServiceMetadata;

public class Main{

    public static void main(final String ... args) throws Exception{
        SwiftServiceParserCommandLineConfig cliConfig = new SwiftServiceParserCommandLineConfig();
        JCommander jCommander = new JCommander(cliConfig);
        jCommander.parse(args);
        jCommander.setProgramName(SwiftServiceParser.class.getSimpleName());

        if (cliConfig.classname == null || (cliConfig.libdirs.isEmpty() && cliConfig.classpath.isEmpty())) {
            jCommander.usage();
            return;
        }
       	ThriftServiceMetadata metadata = SwiftServiceParser.parse(cliConfig.classname, 
       			cliConfig.recursive, 
       			cliConfig.libdirs.toArray(new String[cliConfig.libdirs.size()]),
       			cliConfig.classpath.split(";"));
       	SwiftServiceParser.output(metadata, System.out);
        
    }
}
